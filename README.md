# json-schema-discovery
###### Java 1.8+ Support
Auto discovery of java classes annotated with ```@JsonSchema```, which are placed into a JsonSchemaRepository.

Validation of JSON is also included using the bean validation annotation ```@ValidJson```.

## Example
Java Backing bean of the actual data that should comply to a JSON schema
```
@JsonSchema(name = "person", version = "1.0", location = "/person-schema-v1.json")
    private static class Person {
        private String firstName;

        private String lastName;
    }
```
Or location from url
```
@JsonSchema(name = "person", version = "1.0", location = "http://mysite/schemas/person-schema-v1.json")
    private static class Person {
        private String firstName;

        private String lastName;
    }
```

The JSON schema itself which resides on the classpath at the location specified in the location attribute of the ```@JsonSchema``` annotation.
```
{
  "title": "Person",
  "type": "object",
  "properties": {
    "firstName": {
      "type": "string"
    },
    "lastName": {
      "type": "string"
    },
    "age": {
      "description": "Age in years",
      "type": "integer",
      "minimum": 0
    }
  },
  "required": ["firstName", "lastName"]
}
```
