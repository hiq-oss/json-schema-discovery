package se.hiq.oss.json.schema.integration;

import se.hiq.oss.json.schema.JsonSchema;

@JsonSchema(name = "some", version = "1.0", location = "/person-schema-v1.json")
public class SchemaClass {
}
