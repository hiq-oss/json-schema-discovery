package se.hiq.oss.json.schema.repo;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class JsonSchemaRepositoryFactoryImplTest {

    @Mock
    private ObjectMapper objectMapper;

    private JsonSchemaRepositoryFactoryImpl factory = new JsonSchemaRepositoryFactoryImpl(objectMapper);

    @Test
    public void create() {
        JsonSchemaRepository repository = factory.create();

        assertThat(repository, notNullValue());
    }
}
