package se.hiq.oss.json.schema.integration;

import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import static org.junit.Assert.*;
import org.junit.Test;

import se.hiq.oss.json.schema.JsonSchemaDiscoverer;
import se.hiq.oss.json.schema.repo.JsonSchemaRegistration;
import se.hiq.oss.json.schema.repo.JsonSchemaRepository;

public class JsonSchemaDiscovererIT {

    @Test
    public void findAnnotationsConsiderMetaAnnotation() {
        JsonSchemaDiscoverer discoverer = new JsonSchemaDiscoverer(new ObjectMapper(), true);
        JsonSchemaRepository repo = discoverer.discoverSchemas("se.hiq.oss.json.schema.integration");
        Optional<JsonSchemaRegistration> registration = repo.getSchemaRegistration("some", "1.0");

        assertTrue(registration.isPresent());
        assertEquals(SchemaClass.class, registration.get().getSerDeClass());

        registration = repo.getSchemaRegistration("extended", "1.0");

        assertTrue(registration.isPresent());
        assertEquals(AnotherSchemaClass.class, registration.get().getSerDeClass());

    }

    @Test
    public void findAnnotations() {
        JsonSchemaDiscoverer discoverer = new JsonSchemaDiscoverer(new ObjectMapper());
        JsonSchemaRepository repo = discoverer.discoverSchemas("se.hiq.oss.json.schema.integration");
        Optional<JsonSchemaRegistration> registration = repo.getSchemaRegistration("some", "1.0");

        assertTrue(registration.isPresent());
        assertEquals(SchemaClass.class, registration.get().getSerDeClass());

        registration = repo.getSchemaRegistration("extended", "1.0");

        assertFalse(registration.isPresent());

    }
}
