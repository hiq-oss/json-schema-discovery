package se.hiq.oss.json.schema.repo;

public interface JsonSchemaRepositoryFactory {

    JsonSchemaRepository create();
}
