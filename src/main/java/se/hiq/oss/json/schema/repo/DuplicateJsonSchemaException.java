package se.hiq.oss.json.schema.repo;


public class DuplicateJsonSchemaException extends RuntimeException {

    public DuplicateJsonSchemaException(final String message) {
        super(message);
    }
}
