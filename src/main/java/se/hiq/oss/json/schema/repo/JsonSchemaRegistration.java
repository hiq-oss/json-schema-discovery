package se.hiq.oss.json.schema.repo;


import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.main.JsonSchema;
import se.hiq.oss.json.schema.validation.impl.JsonSchemaValidator;


public interface JsonSchemaRegistration {

    Class getSerDeClass();

    JsonSchema getJsonSchema();

    JsonSchemaValidator<String> getStringValidator();

    JsonSchemaValidator<JsonNode> getNodeValidator();
}
