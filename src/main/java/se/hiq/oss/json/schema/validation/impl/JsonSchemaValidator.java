package se.hiq.oss.json.schema.validation.impl;


public interface JsonSchemaValidator<T> {
    void validate(T json);
}
