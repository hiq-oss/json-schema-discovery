package se.hiq.oss.json.schema.validation.impl;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import se.hiq.oss.json.schema.validation.InvalidJsonException;
import se.hiq.oss.json.schema.validation.ValidJson;
import se.hiq.oss.qa.annotation.CoverageIgnore;



/**
 * JSON Schema Validator that validates a JSON Node.
 * <p>
 * New instances needs to initialized by invoking the initialize method.
 */
public class JsonSchemaNodeValidatorImpl extends AbstractJsonValidator
        implements JsonSchemaValidator<JsonNode>, ConstraintValidator<ValidJson, JsonNode> {

    @CoverageIgnore(justification = "Same as JsonSchemaStringValidatorImpl")
    @Override
    public void validate(JsonNode node) {
        ProcessingReport processingReport = isJsonValid(node);
        if (!processingReport.isSuccess()) {
            throw new InvalidJsonException(processingReport, node.asText() + " " + toErrorMessage(processingReport));
        }
    }

    @CoverageIgnore(justification = "Same as JsonSchemaStringValidatorImpl")
    @Override
    public final boolean isValid(final JsonNode value,
                                 final ConstraintValidatorContext context) {
        ProcessingReport processingReport = isJsonValid(value);
        boolean success = processingReport.isSuccess();
        if (!success) {
            setErrorConstraint(processingReport, context);
        }
        return success;
    }
}
