package se.hiq.oss.json.schema.repo;


import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonSchemaRepositoryFactoryImpl implements JsonSchemaRepositoryFactory {

    private ObjectMapper objectMapper;

    public JsonSchemaRepositoryFactoryImpl(final ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public JsonSchemaRepository create() {
        return new JsonSchemaRepositoryImpl(objectMapper);
    }
}
